<?php


class imageEditorController{

    public $m;
    public $n;
    public $char_function;
    public $param;
    public $image;
    public $color;
    
    function __construct(){
        $this->initVars();
    }
    function initVars(){
        $this->m = 0;
        $this->n = 0;
        $this->char_function = "";
        $this->param = [];
        $this->default_color = "O";
        $this->limit_y=false;
        $this->limit_x=false;
    }

    function command($command){
        $this->__getCommand($command);
        return $this->executeFunction();
    }

    private function __getCommand($command){
        $this->char_function = $this->__getCharFunction($command);
        $this->params = $this->__getParams($command);
    }

    private function __getCharFunction($command){
        return substr($command,0,1);
    }

    private function __getParams($command){
        return explode(" ", substr($command,2));
    }    

    function executeFunction(){
        switch($this->char_function){
            case "I":
                return $this->initImage();
            case "S":
                return $this->show();
            case "C":
                return $this->clear();
            case "L":
                return $this->color();
            case "V":
                return $this->drawVertical();
            case "H":
                return $this->drawHorizontal();
            case "F":
                return $this->fill();
            case "X":
                return $this->finish();
        }
    }

    function initImage(){
        $this->__setSize();
        $this->color = $this->default_color;
        $this->__modifiedRegion(0, 0, $this->m, $this->n);
    }

    private function __setSize(){
        $this->m = $this->params[0];
        $this->n = $this->params[1];
    }

    function clear(){
        $this->color = $this->default_color;
        $this->__modifiedRegion(1, 1, $this->m, $this->n);
    }

    function color(){
        $this->color = $this->params[2];
        $this->__modifiedPixel($this->params[0], $this->params[1], $this->color);
    }

    function drawVertical(){
        $this->color = $this->params[3];
        $this->__modifiedRegion($this->params[0], $this->params[1], $this->params[0], $this->params[2]);
    }

    function drawHorizontal(){
        $this->color = $this->params[3];
        $this->__modifiedRegion($this->params[0], $this->params[2], $this->params[1], $this->params[2]);
    }

    function show(){
        $show = "";
        for($j=1; $j<=$this->n; $j++)
        {
            for($i=1; $i<=$this->m; $i++){
                $show .= $this->image[$j][$i];
            }
            $show .= "</br>";
        }
        $show .= "</br>";
        print($show);
    }

    function fill(){
        $this->color = $this->params[2];
        $actual_color =  $this->image[$this->params[1]][$this->params[0]];
        $this->find_next_pixel($this->params[0], $this->params[1], $actual_color);
    }

    function find_next_pixel($x,$y, $color){
        $this->__modifiedPixel($x, $y, $this->color);
        if($x>=1 && $x<=$this->m && $y>=1 && $y<=$this->n){
            if($this->image[$y][$x+1] == $color){
                $this->find_next_pixel($x+1, $y, $color);
            }
            if($this->image[$y+1][$x] == $color){
                $this->find_next_pixel($x, $y+1, $color);
            }
            if($this->image[$y-1][$x] == $color){
                $this->find_next_pixel($x, $y-1, $color);
            }
            if($this->image[$y][$x-1] == $color){
                $this->find_next_pixel($x-1, $y, $color);
            }
            return false;
        }
    }

    function finish(){
        $this->initVars();
    }

    private function __modifiedRegion($x1, $y1, $x2, $y2){
        for($j=$y1;$j<=$y2;$j++)
            for($i=$x1;$i<=$x2;$i++)
                $this->__modifiedPixel($i, $j, $this->color);
    }

    private function __modifiedPixel($i, $j, $color){
        $this->image[$j][$i] = $color;
    }

}


$image = New imageEditorController();
print("I 5 8 </br>");
$image->command("I 5 8");
print("S</br>");
$image->command("S");
print("L 1 1 W </br>");
$image->command("L 1 1 W");
print("S</br>");
$image->command("S");
print("V 2 3 8 W</br>");
$image->command("V 2 3 8 W");
print("S</br>");
$image->command("S");
print("H 2 4 2 Z</br>");
$image->command("H 2 4 2 Z");
print("S</br>");
$image->command("S");
print("F 4 5 Y</br>");
$image->command("F 4 5 Y");
print("S</br>");
$image->command("S");
print("X</br>");
$image->command("X");
print("S</br>");
$image->command("S");